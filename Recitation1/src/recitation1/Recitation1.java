/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recitation1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

/**
 *
 * @author jschmitz
 */
public class Recitation1 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Button hBtn = new Button();
        Button bBtn = new Button();
        hBtn.setText("Hello World");
        hBtn.setOnAction(new MessageButtonHandler("Hello World!"));
        bBtn.setText("Goodbye Cruel World!");
        bBtn.setOnAction(new MessageButtonHandler("Goodbye Cruel World!"));
        
        FlowPane root = new FlowPane();
        root.getChildren().addAll(hBtn, bBtn);
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public class MessageButtonHandler implements EventHandler<ActionEvent> {

        private String message;
        
        public MessageButtonHandler(String message){
            this.message = message;
        }
        
        @Override
        public void handle(ActionEvent event) {
            System.out.println(message);
        }
    
    
    }
    
    
}
